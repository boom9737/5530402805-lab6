package kitteerapakorn.attaphon.lab6;

import java.awt.*;
import javax.swing.*;

public class BankAccount {
	
	// ���ҧ object ��ҧ�
	protected static JFrame window;
	protected static JPanel p1;
	protected static JPanel p2;
	
	protected static JButton WithdrawButton;
	protected static JButton DepositButton;
	protected static JButton ExitButton;
	
	public static void main(String[] args) {
		window = new JFrame("Simple Bank Account"); // ��駪��� frame
		p1 = new JPanel();
		p2 = new JPanel();
		
		p1.setLayout(new GridLayout(3, 2)); // ��˹� Panel p1 ����繵��ҧ 3x2
		p1.add(new JLabel("Previous Balance: ")); // ����ͤ���
		p1.add(new JTextField("1000",15)); // �����ͧ��ͤ��� �բ�ͤ���������� "1000" �����  15 ����ѡ��
		p1.add(new JLabel("Amount: ")); // ����ͤ���
		p1.add(new JTextField(15));// �����ͧ��ͤ��� �����  15 ����ѡ��
		p1.add(new JLabel("Current Balance: ")); // ����ͤ���
		p1.add(new JTextField(15));// �����ͧ��ͤ��� �����  15 ����ѡ��
		
		WithdrawButton = new JButton("Withdraw"); // ��駪��ͻ���
		DepositButton = new JButton("Deposit"); // ��駪��ͻ���
		ExitButton = new JButton("Exit"); // ��駪��ͻ���

		p2.add(WithdrawButton); // ���������� � Panel p2
		p2.add(DepositButton); // ���������� � Panel p2
		p2.add(ExitButton); // ���������� � Panel p2

		window.add(p1, BorderLayout.NORTH); // ��� Panel p1 ����� frame window ��ǹ��
		window.add(p2, BorderLayout.SOUTH); // ��� Panel p2 ����� frame window ��ǹ��ҧ
		window.pack(); // �Ѵ���¹��觵�ҧ�� frame window �ѵ��ѵ�
		window.setLocation(200, 200); // ��˹����˹� �Ŵ�frame window
		window.setVisible(true); // ����� �ʴ� frame window
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // ����÷ӧҹ���������͡��Դ
	}
}
