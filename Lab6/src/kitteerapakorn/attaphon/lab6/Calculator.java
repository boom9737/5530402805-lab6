package kitteerapakorn.attaphon.lab6;

import javax.swing.*;

import java.awt.*;

public class Calculator {

	public static void main(String[] args) {
		JFrame calculator = new JFrame("calculator"); // ���ҧ frame ���
		JPanel num = new JPanel(); // ���ҧ Panel �������Ţ��Ф���ͧ����
		num.setLayout(new GridLayout(4, 4, 10, 10)); // �絾�������繵��ҧ
														// 4x4
														// ��������ҧ�ͧ���Ъ�ͧ
														// 10x10
		JButton[] buttonNum = new JButton[10]; // ���ҧ�����Ţ
		JPanel[] nPanel = new JPanel[16]; // ���ҧ Panel �����ѧ���ժ�ͧ
		JButton[] symbols = new JButton[6]; // ���ҧ��������ͧ����

		// ��˹���������ͧ��������ժ��͵�ҧ�
		symbols[0] = new JButton("      " + "+" + "      ");
		symbols[1] = new JButton("      " + "-" + "      ");
		symbols[2] = new JButton("      " + "*" + "      ");
		symbols[3] = new JButton("      " + "/" + "      ");
		symbols[4] = new JButton("      " + "%" + "      ");
		symbols[5] = new JButton("      " + "=" + "      ");

		// ��˹�����Ţ����������Ţ
		for (int i = 1; i <= 16; i++) {
			nPanel[i - 1] = new JPanel();
			if (i <= 10) {
				buttonNum[i - 1] = new JButton("      " + (i % 10) + "      ");
				nPanel[i - 1].add(buttonNum[i - 1]); // ������ŧ� Panel
														// �����ѧ
				nPanel[i - 1].setBackground(Color.GRAY); // ��˹��� Panel
															// �����ѧ
				num.add(nPanel[i - 1]); // ���ŧ� Panel ���
			} else {
				nPanel[i - 1].setBackground(Color.PINK); // ��˹��� Panel
															// �����ѧ
				nPanel[i - 1].add(symbols[i - 11]); // ������ŧ� Panel
													// �����ѧ
				num.add(nPanel[i - 1]); // ���ŧ� Panel ���
			}
		}
		JTextField text = new JTextField(15); // ���ҧ��ͧ��͹��ͤ���
		calculator.add(text, BorderLayout.NORTH); // �Ӫ�ͧ��ͤ����������ǹ��ҧ��
													// frame
		calculator.add(num, BorderLayout.SOUTH); // �� Panel ����Ţ���
													// ����ͧ��������������ҧ�ͧ
													// frame
		calculator.setLocation(300, 300); // ��˹����˹�㹡���ʴ������
		calculator.pack(); // �Ѵ���§��觵�ҧ�������� frame ���ѵ��ѵ�
		calculator.setVisible(true); // ������ʴ� frame calculator
		calculator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // �����������͡��Դ
	}
}